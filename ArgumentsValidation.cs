﻿using System;
using System.Collections.Generic;

namespace ITask3
{
    public class ArgumentsValidation
    {
        private static string[] regions = { "en_US", "ru_RU", "be_BY" };
        private static string[] arguments;

        public static bool ArgumentsIsValid(string[] _arguments)
        {
            arguments = _arguments;
            if (ArgumentsCountIsValid())
                if (RegionIsValid() && RecordsCountIsValid() && ErrorRateIsValid())
                    return true;
            return false;
        }

        private static bool ArgumentsCountIsValid()
        {
            if (arguments.Length == 0)
            {
                Console.WriteLine("Аргументы не заданы.");
                return false;
            }
            if (arguments.Length < 2)
            {
                Console.WriteLine("Необходимо ввести минимум 2 аргумента.");
                return false;
            }
            return true;
        }

        private static bool RegionIsValid()
        {
            foreach (var region in regions)
                if (arguments[0] == region) return true;
            Console.WriteLine("Введённый регион не поддерживается.");
            return false;
        }

        private static bool RecordsCountIsValid()
        {
            int count;
            try { count = int.Parse(arguments[1]); }
            catch
            {
                Console.WriteLine("Вторым аргументом должно быть целое положительное число.");
                return false;
            }
            if (count < 0)
            {
                Console.WriteLine("Значение второго аргумента должно быть больше 0.");
                return false;
            }
            return true;
        }

        private static bool ErrorRateIsValid()
        {
            if (arguments.Length > 2)
            {
                double errorRate;
                try { errorRate = Convert.ToDouble(arguments[2]); }
                catch
                {
                    Console.WriteLine("Третьим аргументом должно быть вещественное положительное число.");
                    return false;
                }
                if (errorRate < 0)
                {
                    Console.WriteLine("Значение третьего аргумента должно быть больше 0.");
                    return false;
                }
            }
            return true;
        }
    }
}

