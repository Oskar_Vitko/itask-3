﻿using System;
using System.Text;

namespace ITask3
{
    public class Program
    { 
        public static string region;
        public static int entryCount;
        public static float errorCount = 0;
        private static Random random = new Random();

        private static void Main(string[] args)
        {
            if (ArgumentsValidation.ArgumentsIsValid(args))
            {
                Console.OutputEncoding = Encoding.UTF8;
                SetValues(args);
                Entry.ShowEntries();
            }
        }

        private static void SetValues(string[] arguments)
        {
            region = $"{arguments[0][0]}{arguments[0][1]}";
            entryCount = int.Parse(arguments[1]);
            if (arguments.Length > 2)
                SetErrorCount(Convert.ToDouble(arguments[2]));
        }


        private static void SetErrorCount(double errorRate)
        {
            errorCount = Convert.ToInt32(errorRate - errorRate % 1);
            if (random.NextDouble() <= errorRate % 1)
                errorCount++;
        }
    }
}
