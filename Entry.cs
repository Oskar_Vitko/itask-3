﻿using Bogus;
using System;

namespace ITask3
{
    public class Entry
    {
        private static Faker faker;
        private static Random random = new Random();

        public static void ShowEntries()
        {
            faker = new Faker(Program.region);
            for (int i = 0; i < Program.entryCount; i++)
                Console.WriteLine(CreateEntry());
        }

        private static string CreateEntry()
        {
            string[] entry = { GetFio(), GetAddress(), GetPhoneNumber()};
            if(Program.errorCount >0)
            Error.GenerateError(entry);
            return $"{entry[0]}; {entry[1]}; {entry[2]}";
        }

        private static string GetFio()
        {
            return faker.Name.FullNameWithMiddle();
        }

        private static string GetAddress()
        {
            return $"{faker.Address.ZipCode()}, {faker.Address.DefaultCountry()}," +
                $" {faker.Address.City()}, {faker.Address.StreetName()}," +
                $" {faker.Address.BuildingNumberWithBuildingSuffix()}";
        }

        private static string GetPhoneNumber()
        {
            string[] beCode =  { "+375(29) ", "+375(33) ", "+375(44) ", "+375(25) " };
            string[] ruCode = { "8(967) ","7(915) ","8(800) ","7(929) "};
            string phone = faker.Phone.PhoneNumber();
            if (Program.region == "be") phone = beCode[random.Next(0,beCode.Length)] + phone.Substring(phone.Length - 9, 9);
            else if (Program.region == "ru") phone = ruCode[random.Next(0, ruCode.Length)] + phone.Substring(phone.Length - 9, 9);
            return phone;
        }

    }
}
