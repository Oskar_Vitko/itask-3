﻿using System;
using System.Collections.Generic;

namespace ITask3
{
    public class Error
    {
        private static int currLineID;
        private static string currentLine;
        private static string[] entry;
        private static Random random = new Random();

        public static string[] GenerateError(string[] _entry)
        {
            for(int i=0; i<Program.errorCount; i++)
            {
                entry = _entry;
                SetCurrentLine();
                SetErrorType();
                entry[currLineID] = currentLine;
            }
            return entry;
        }

        private static void SetCurrentLine()
        {
            currLineID = random.Next(0, entry.Length);
            currentLine = entry[currLineID];
        }

        private static void SetErrorType()
        {
            int rnd = random.Next(0, 3);
            if (rnd == 0) DeleteRandom();
            else if (rnd == 1) AddRandom();
            else if (rnd == 2) SwapSymbols();
        }

        private static void DeleteRandom()
        {
            if (currentLine.Length > 1)
            {
                int symPos = random.Next(0, currentLine.Length);
                currentLine = currentLine.Remove(symPos, 1);
            }
        }

        private static void AddRandom()
        {
            if (currentLine.Length < 80)
            {
                int symPos = random.Next(0, currentLine.Length);
                string symbol = new Bogus.DataSets.Lorem(Program.region).AlphaNumeric();
                currentLine = currentLine.Insert(symPos, symbol);
            }
        }

        private static void SwapSymbols()
        {
            if (currentLine.Length > 2)
            {
                int symPos = random.Next(0, currentLine.Length);
                int step = symPos != currentLine.Length - 1 ? 1 : -1;
                string temp = currentLine[symPos].ToString();
                currentLine = currentLine.Remove(symPos, 1);
                currentLine = currentLine.Insert(symPos + step, temp);
            }
        }
    }
}
